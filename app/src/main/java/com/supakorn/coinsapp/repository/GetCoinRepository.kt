package com.supakorn.coinsapp.repository

import com.supakorn.coinsapp.model.CoinResponse
import com.supakorn.coinsapp.networking.CoinApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

interface GetCoinRepository {
    suspend fun getCoin(limit: String, nextRank: String?): Flow<Result<CoinResponse>>
}

class GetCoinRepositoryImpl(private val api: CoinApi) : GetCoinRepository {
    companion object {
        const val ERROR_MESSAGE = "CoinRepository failed to get data"
    }

    override suspend fun getCoin(limit: String, nextRank: String?): Flow<Result<CoinResponse>> {
        return flow {
            val response = api.getCoin(limit, nextRank)

            if (response.isSuccessful && response.body() != null) {
                emit(Result.success(response.body()) as Result<CoinResponse>)
            } else {
                val resultFail: Result<CoinResponse> = Result.failure(Throwable(ERROR_MESSAGE))
                emit(resultFail)
            }
        }.catch { error ->
            emit(Result.failure(error))
        }
    }
}