package com.supakorn.coinsapp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.supakorn.coinsapp.R
import com.supakorn.coinsapp.main.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }
}