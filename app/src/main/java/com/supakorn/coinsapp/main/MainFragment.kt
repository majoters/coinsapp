package com.supakorn.coinsapp.main

import android.app.Activity
import android.content.Intent
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.supakorn.coinsapp.adapter.CoinAdapter
import com.supakorn.coinsapp.adapter.LoadingAdapter
import com.supakorn.coinsapp.databinding.MainFragmentBinding
import com.supakorn.coinsapp.model.CoinContentModel
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.search_header_layout.*
import java.util.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
        const val SPEECH_REQUEST_CODE = 0
    }
    private lateinit var binding: MainFragmentBinding
    private val viewModel: MainViewModel by viewModel()
    private val coinAdapter by lazy {
        CoinAdapter()
    }
    private val loadingAdapter by lazy {
        LoadingAdapter()
    }

    private var coinList = mutableListOf<CoinContentModel>()
    private var isPullRefresh = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialView()
        initialViewModel()
        initialListener()
        viewModel.getAllCoins()
    }

    private fun initialViewModel() {
        viewModel.onGetAllCoins().observe(viewLifecycleOwner, { coin ->
            swipeLayout?.isRefreshing = false
            isPullRefresh = false
            if (!coin.isNullOrEmpty()) {
                attachTempList(coin)
                attachAdapter(coin)
                toggleRecyclerView()
            } else {
                toggleErrorRecyclerView()
            }
        })

        viewModel.onErrorCoins().observe(viewLifecycleOwner, {
            toggleErrorRecyclerView()
        })

        viewModel.onShowListLoading().observe(viewLifecycleOwner, {
            loadingAdapter.showLoading()
        })

        viewModel.onHideListLoading().observe(viewLifecycleOwner, {
            loadingAdapter.hideLoading()
        })
    }

    private fun initialView() = with(binding) {
        context?.let { coinAdapter.setContext(it) }
        val config = ConcatAdapter.Config.Builder()
            .setIsolateViewTypes(false)
            .build()
        val allAdapter = ConcatAdapter(
            config,
            coinAdapter,
            loadingAdapter
        )
        coinsRecyclerView.apply {
            adapter = allAdapter
            layoutManager = LinearLayoutManager(context)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0) {
                        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                        val visibleItemCount = (layoutManager.findLastCompletelyVisibleItemPosition() + 1)
                        if (visibleItemCount == layoutManager.itemCount) {
                            viewModel.getAllCoins()
                        }
                    }
                }
            })
        }
        coinAdapter.setOnItemClick { item, position ->
            Log.d("click", "title: ${item.title} position: $position")
        }
    }

    private fun initialListener() {
        swipeLayout?.setOnRefreshListener {
            coinAdapter.clearItemList()
            binding.noSearchResultsFoundText.visibility = View.INVISIBLE
            viewModel.getAllCoins()
            isPullRefresh = true
        }
        searchEditText.doOnTextChanged { text, _, _, _ ->
            attachAdapter(coinList)
            val query = text.toString().lowercase(Locale.getDefault())
            filterWithQuery(query)
            toggleImageView(query)
        }
        voiceSearchQuery.setOnClickListener {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
                putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
                )
            }
            startActivityForResult(intent, SPEECH_REQUEST_CODE)
        }

        clearSearchQuery.setOnClickListener {
            searchEditText.setText("")
            attachAdapter(coinList)
            toggleImageView("")
        }
    }

    private fun attachTempList(list: List<CoinContentModel>) {
        coinList.clear()
        coinList.addAll(list)
    }

    private fun attachAdapter(list: List<CoinContentModel>) {
        coinAdapter.clearItemList()
        coinAdapter.setItemList(list)
    }

    private fun filterWithQuery(query: String) {
        if (query.isNotEmpty()) {
            val filteredList: List<CoinContentModel> = onQueryChanged(query)
            attachAdapter(filteredList)
            if (filteredList.isNullOrEmpty()) {
                toggleEmptySearchRecyclerView()
            } else {
                toggleSearchRecyclerView()
            }

        } else if (query.isEmpty()) {
            attachAdapter(coinList)
        }
    }

    private fun onQueryChanged(filterQuery: String): List<CoinContentModel> {
        val filteredList = ArrayList<CoinContentModel>()
        for (currentCoin in coinList) {
            if (
                currentCoin.title?.lowercase(Locale.getDefault())
                    ?.contains(filterQuery) == true ||
                currentCoin.description?.lowercase(Locale.getDefault())
                    ?.contains(filterQuery) == true ||
                currentCoin.symbol?.lowercase(Locale.getDefault())
                    ?.contains(filterQuery) == true ||
                currentCoin.slug?.lowercase(Locale.getDefault())?.contains(filterQuery) == true ||
                currentCoin.uuid?.lowercase(Locale.getDefault())?.contains(filterQuery) == true
            ) {
                filteredList.add(currentCoin)
            }
        }
        return filteredList
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val spokenText: String? =
                data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).let { results ->
                    results?.get(0)
                }
            searchEditText.setText(spokenText)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun toggleRecyclerView() {
        binding.coinsRecyclerView.visibility = View.VISIBLE
        binding.noCoinsFoundText.visibility = View.INVISIBLE
    }

    private fun toggleErrorRecyclerView() {
        binding.coinsRecyclerView.visibility = View.INVISIBLE
        binding.noCoinsFoundText.visibility = View.VISIBLE
    }

    private fun toggleSearchRecyclerView() {
        binding.coinsRecyclerView.visibility = View.VISIBLE
        binding.noSearchResultsFoundText.visibility = View.INVISIBLE
    }

    private fun toggleEmptySearchRecyclerView() {
        binding.coinsRecyclerView.visibility = View.INVISIBLE
        binding.noSearchResultsFoundText.visibility = View.VISIBLE
    }

    private fun toggleImageView(query: String) {
        if (query.isNotEmpty()) {
            clearSearchQuery.visibility = View.VISIBLE
            voiceSearchQuery.visibility = View.INVISIBLE
        } else if (query.isEmpty()) {
            clearSearchQuery.visibility = View.INVISIBLE
            voiceSearchQuery.visibility = View.VISIBLE
        }
    }
}