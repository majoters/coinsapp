package com.supakorn.coinsapp.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.supakorn.coinsapp.model.CoinContentModel
import com.supakorn.coinsapp.usecase.GetAllCoinUseCase
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class MainViewModel(
    private val getAllCoinUseCase: GetAllCoinUseCase
) : ViewModel() {
    private val getAllCoins = MutableLiveData<List<CoinContentModel>>()
    private val errorCoins = MutableLiveData<Unit>()
    private val showListLoading = MutableLiveData<Unit>()
    private val hideListLoading = MutableLiveData<Unit>()

    fun onGetAllCoins() = getAllCoins
    fun onErrorCoins() = errorCoins
    fun onShowListLoading() = showListLoading
    fun onHideListLoading() = hideListLoading

    fun getAllCoins() {
        viewModelScope.launch {
            getAllCoinUseCase.execute()
                .onStart {
                    showListLoading.value = Unit
                }
                .catch {
                    errorCoins.value = Unit
                }.collect { coin ->
                    hideListLoading.value = Unit
                    getAllCoins.value = coin
                }
        }
    }
}