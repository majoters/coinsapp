package com.supakorn.coinsapp.adapter

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.supakorn.coinsapp.databinding.CoinItemBinding
import com.supakorn.coinsapp.databinding.CoinItemRightBinding
import com.supakorn.coinsapp.extensions.loadUrl
import com.supakorn.coinsapp.model.CoinContentModel
import com.supakorn.coinsapp.model.CoinViewType


class CoinAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val TYPE_DESCRIPTION = 0
        private const val TYPE_SIMPLE = 1
    }

    private lateinit var context: Context
    private var itemClickListener: ((item: CoinContentModel, position: Int) -> Unit)? = null
    private val coinList = mutableListOf<CoinContentModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_DESCRIPTION -> {
                val binding = CoinItemBinding
                    .inflate(LayoutInflater.from(parent.context), parent, false)
                CoinViewHolder(binding)
            }
            TYPE_SIMPLE -> {
                val binding = CoinItemRightBinding
                    .inflate(LayoutInflater.from(parent.context), parent, false)
                CoinRightSideViewHolder(binding)
            }
            else -> throw IllegalArgumentException("Invalid view type")

        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val element = coinList[position]
        when (holder) {
            is CoinViewHolder -> holder.bind(element, position)
            is CoinRightSideViewHolder -> holder.bind(element, position)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position == 0 -> {
                CoinViewType.COIN_DESCRIPTION.value
            }
            ((position + 1) % 5) == 0 -> {
                CoinViewType.COIN_SIMPLE.value
            }
            else -> {
                CoinViewType.COIN_DESCRIPTION.value
            }
        }
    }

    override fun getItemCount(): Int {
        return coinList.size
    }

    fun setContext(context: Context) {
        this.context = context
    }

    fun setItemList(list: List<CoinContentModel>) {
        coinList.addAll(list)
        notifyDataSetChanged()
    }

    fun clearItemList() {
        coinList.clear()
        notifyDataSetChanged()
    }

    fun setOnItemClick(listener: (item: CoinContentModel, position: Int) -> Unit) {
        this.itemClickListener = listener
    }

    inner class CoinViewHolder(private val binding: CoinItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CoinContentModel, position: Int) = with(binding) {
            titleBitCoin.text = item.title
            descriptionBitcoin.text = if (item.description?.contains("<p>") == true) Html.fromHtml(item.description?.replace("<p>", "")) else item.description
            imageView.loadUrl(item.iconUrl)
            root.setOnClickListener {
                itemClickListener?.invoke(item, position)
            }
        }
    }

    inner class CoinRightSideViewHolder(private val binding: CoinItemRightBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CoinContentModel, position: Int) = with(binding) {
            titleBitCoinRight.text = item.title
            imageView2.loadUrl(item.iconUrl)
            root.setOnClickListener {
                itemClickListener?.invoke(item, position)
            }
        }
    }
}