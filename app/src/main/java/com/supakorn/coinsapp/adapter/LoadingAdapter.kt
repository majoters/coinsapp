package com.supakorn.coinsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.supakorn.coinsapp.databinding.CoinItemLoadingBinding
import com.supakorn.coinsapp.model.CoinViewType

class LoadingAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isShowLoading: Boolean = false

    fun showLoading() {
        isShowLoading = true
        notifyDataSetChanged()
    }

    fun hideLoading() {
        isShowLoading = false
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = CoinItemLoadingBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return LoadingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoadingViewHolder -> holder.bind(isShowLoading)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return CoinViewType.LOADING.value
    }

    override fun getItemCount(): Int = 1

    inner class LoadingViewHolder(private val binding: CoinItemLoadingBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(isShow: Boolean) = with(binding) {
            if (isShow) {
                root.visibility = View.VISIBLE
            } else {
                root.visibility = View.GONE
            }
        }
    }
}