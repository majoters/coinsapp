package com.supakorn.coinsapp.networking

import androidx.annotation.WorkerThread
import com.supakorn.coinsapp.model.CoinResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CoinApi {
    @WorkerThread
    @GET("v1/public/coins")
    suspend fun getCoin(
        @Query("limit") limit: String,
        @Query("offset") offset: String? = null
    ): Response<CoinResponse>
}