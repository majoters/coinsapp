package com.supakorn.coinsapp.model

class CoinContentModel {
    var title: String? = ""
    var description: String? = ""
    var symbol: String? = ""
    var slug: String? = ""
    var uuid: String? = ""
    var id: String? = ""
    var rank: String? = ""
    var iconUrl: String? = ""
}