package com.supakorn.coinsapp.model

enum class CoinViewType(val value: Int) {
    COIN_DESCRIPTION(0),
    COIN_SIMPLE(1),
    LOADING(2)
}