package com.supakorn.coinsapp.model

data class CoinResponse(
    val data: Data,
    val status: String = ""
)


data class CoinsItem(
    val symbol: String = "",
    val color: String = "",
    val penalty: Boolean = false,
    val description: String = "",
    val type: String = "",
    val uuid: String = "",
    val circulatingSupply: Double = 0.0,
    val websiteUrl: String = "",
    val allTimeHigh: AllTimeHigh,
    val price: String = "",
    val iconType: String = "",
    val rank: Int = 0,
    val links: List<LinksItem>?,
    val approvedSupply: Boolean = false,
    val id: String = "",
    val iconUrl: String = "",
    val socials: List<SocialsItem>?,
    val slug: String = "",
    val marketCap: Double = 0.0,
    val numberOfMarkets: Int = 0,
    val confirmedSupply: Boolean = false,
    val totalSupply: Double = 0.0,
    val firstSeen: Long = 0,
    val change: Double = 0.0,
    val history: List<String>?,
    val listedAt: Int = 0,
    val volume: Long = 0,
    val name: String = "",
    val numberOfExchanges: Int = 0
)


data class SocialsItem(
    val name: String = "",
    val type: String = "",
    val url: String = ""
)


data class Stats(
    val total: Int = 0,
    val offset: Int = 0,
    val totalExchanges: Int = 0,
    val limit: Int = 0,
    val totalMarkets: Int = 0,
    val totalMarketCap: Double = 0.0,
    val totalHVolume: Double = 0.0,
    val order: String = "",
    val base: String = ""
)


data class Data(
    val stats: Stats,
    val coins: List<CoinsItem>?,
    val base: Base
)


data class LinksItem(
    val name: String = "",
    val type: String = "",
    val url: String = ""
)


data class AllTimeHigh(
    val price: String = "",
    val timestamp: Long = 0
)


data class Base(
    val symbol: String = "",
    val sign: String = ""
)


