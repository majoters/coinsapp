package com.supakorn.coinsapp.module

import com.supakorn.coinsapp.networking.okHttp
import com.supakorn.coinsapp.networking.retrofit
import com.supakorn.coinsapp.networking.CoinApi
import org.koin.dsl.module
import retrofit2.Retrofit

const val BASE_URL = "https://api.coinranking.com/"

val networkModule = module {
    single {
        okHttp()
    }
    single {
        retrofit(BASE_URL)
    }
    single {
        get<Retrofit>().create(CoinApi::class.java)
    }
}