package com.supakorn.coinsapp.module

import com.supakorn.coinsapp.repository.GetCoinRepository
import com.supakorn.coinsapp.repository.GetCoinRepositoryImpl
import com.supakorn.coinsapp.usecase.*
import org.koin.dsl.module

val coinModule = module {
    single<GetCoinRepository> { GetCoinRepositoryImpl(api = get()) }
    single<GetAllCoinUseCase> { GetAllCoinUseCaseImpl(getCoinRepository = get(), getAllCoinCacheUseCase = get()) }
    single<GetAllCoinCacheUseCase> { GetAllCoinCacheUseCaseImpl() }
}