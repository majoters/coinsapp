package com.supakorn.coinsapp.module

import com.supakorn.coinsapp.main.MainFragment
import org.koin.dsl.module

val fragmentModule = module {
    factory { MainFragment() }
}