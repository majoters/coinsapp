package com.supakorn.coinsapp.extensions

import android.widget.ImageView
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.supakorn.coinsapp.R

fun ImageView.loadUrl(url: String?) {

    val imageLoader = ImageLoader.Builder(this.context)
        .componentRegistry { add(SvgDecoder(context)) }
        .build()

    val request = ImageRequest.Builder(this.context)
        .crossfade(true)
        .crossfade(500)
        .placeholder(R.drawable.ic_bitcoin)
        .error(R.drawable.ic_bitcoin)
        .data(url)
        .target(this)
        .build()

    imageLoader.enqueue(request)
}