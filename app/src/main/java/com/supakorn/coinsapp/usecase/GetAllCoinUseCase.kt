package com.supakorn.coinsapp.usecase

import com.supakorn.coinsapp.model.CoinContentModel
import com.supakorn.coinsapp.model.CoinResponse
import com.supakorn.coinsapp.repository.GetCoinRepository
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*

interface GetAllCoinUseCase {
    suspend fun execute(): Flow<List<CoinContentModel>?>
}

class GetAllCoinUseCaseImpl(
    private val getCoinRepository: GetCoinRepository,
    private val getAllCoinCacheUseCase: GetAllCoinCacheUseCase
) : GetAllCoinUseCase {
    companion object {
        const val LIMIT = "10"
    }

    @FlowPreview
    override suspend fun execute(): Flow<List<CoinContentModel>?> {
        getAllCoinCacheUseCase.getCoins()?.let {
            getAllCoinCacheUseCase.getNextRank()?.let { nextRank ->
                return loadCoins(LIMIT, nextRank)
            } ?: run {
                return flowOf(loadCoinsFromCache())
            }
        } ?: run {
            return loadCoins(LIMIT)
        }
    }

    private fun loadCoins(limit: String, nextRank: String? = null) = flow {
        getCoinRepository.getCoin(limit, nextRank).collect { response ->
            response.getOrNull()?.let {
                saveCacheCoins(mapToCoinContentModel(it))
                emit(loadCoinsFromCache())
            }
        }
    }

    private fun saveCacheCoins(coinItems: List<CoinContentModel>) {
        getAllCoinCacheUseCase.apply {
            addCoins(coinItems)
            saveNextRank(coinItems[(coinItems.size - 1)].rank)
        }
    }

    private fun mapToCoinContentModel(allCoin: CoinResponse): List<CoinContentModel> {
        val coinList = mutableListOf<CoinContentModel>()
        allCoin.data.coins?.map { coinsItem ->
            coinList.add(CoinContentModel().apply {
                this.title = coinsItem.name
                this.description = coinsItem.description
                this.symbol = coinsItem.symbol
                this.slug = coinsItem.slug
                this.uuid = coinsItem.uuid
                this.id = coinsItem.id
                this.rank = "${coinsItem.rank}"
                this.iconUrl = coinsItem.iconUrl
            })
        }
        return coinList.toList()
    }

    private fun loadCoinsFromCache(): List<CoinContentModel>? {
        return getAllCoinCacheUseCase.getCoins()
    }
}