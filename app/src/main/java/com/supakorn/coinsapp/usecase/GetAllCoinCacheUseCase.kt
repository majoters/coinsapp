package com.supakorn.coinsapp.usecase

import com.supakorn.coinsapp.model.CoinContentModel

interface GetAllCoinCacheUseCase {
    fun getCoins(): List<CoinContentModel>?
    fun addCoins(coinItems: List<CoinContentModel>)
    fun getNextRank(): String?
    fun saveNextRank(nextRank: String?)
}
class GetAllCoinCacheUseCaseImpl : GetAllCoinCacheUseCase {

    private var coinItems: MutableList<CoinContentModel>? = null
    private var nextRank: String? = null

    override fun getCoins(): List<CoinContentModel>? {
        return coinItems
    }

    override fun addCoins(coinItems: List<CoinContentModel>) {
        this.coinItems?.let {
            this.coinItems?.addAll(coinItems)
        } ?:run {
            this.coinItems = mutableListOf()
            this.coinItems?.addAll(coinItems)
        }
    }

    override fun getNextRank(): String? {
        return nextRank
    }

    override fun saveNextRank(nextRank: String?) {
        this.nextRank = nextRank
    }
}